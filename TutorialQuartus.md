# Tutorial para instalação do Quartus 18.1 no Linux #

### Download do programa ###
O primeiro passo é baixar o Quartus 18.1 utilizando o link abaixo:

[Download](https://fpgasoftware.intel.com/18.1/?edition=lite)

### Instalando o Quartus ###
* Descompactar o arquivo:
> $ tar -xf Quartus-lite-18.1.0.625-linux.tar
* Ir para o diretório "components":
> $ cd components
* Dar permissão de execução ao arquivo:
> $ chmod +x QuartusLiteSetup-18.1.0.625-linux.run
* Iniciar a instalação:
> $ ./QuartusLiteSetup-18.1.0.625-linux.run
* Siga a instalação até o final

### Configurando o ModelSim ###	
* Vá para o diretório "intelFPGA_lite/18.1/modelsim_ase"
> $ cd ~/intelFPGA_lite/18.1/modelsim_ase
* Baxe o arquivo lib32-freetype2-2.5.0.1.tar.xz
> $ wget https://bitbucket.org/Allyson/quartus/raw/1ec241f58a64d312ff8e846db36954411cfd02b8/lib32-freetype2-2.5.0.1.tar.xz
* Descompacte o arquivo:
> $ tar -xf lib32-freetype2-2.5.0.1.tar.xz
* Edite o arquivo "vco"
> $ chmod 766 vco; vim vco;chmod 555 vco
* Procure por "linux_rh60" e troque por "linux"
>  :/linux_rh60
* Saia e salve o arquivo
> :wq
* Instale a arquitetura 32 bits
> $ sudo dpkg --add-architecture i386
* Atualize os pacotes
> $ sudo apt-get update
* Instale os seguintes pacotes:
> $ sudo apt install libxft2:i386 libxext6:i386 libncurses5:i386 bzip2:i386
* Vá para o diretório "~/intelFPGA_lite/18.1/quartus/adm"
> $cd ~/intelFPGA_lite/18.1/quartus/adm
* Edite o arquivo qenv.sh
> $ chmod 766 qenv.sh; vim qenv.sh;chmod 555 qenv.sh
* Coloque o seguinte comando:
> export LD_LIBRARY_PATH=/home/<USUARIO>/intelFPGA/18.1/modelsim_ase/lib32:/$LD_LIBRARY_PATH 
* na linha anterior ao 
> if test ! "${QUARTUS_QENV-UNSET}" != UNSET ; then
* Lembre de trocar <USUARIO> para o usuario do seu sistema
	
